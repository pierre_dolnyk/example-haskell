import Data.List (transpose, (\\))

type Grid     = Matrix Value
type Matrix a = [Row a]
type Row a    = [a]
type Value    = Char
type Choices  = [Value]

blank :: Grid
blank = replicate 9 $ replicate 9 '.'

rows :: Matrix a -> [Row a]
rows = id

cols :: Matrix a -> [Row a]
cols = transpose

boxs :: Matrix a -> [Row a]
boxs = unpack . map cols . pack
        where 
            pack = split . map split
            split = chop 9
            unpack = map concat . concat

chop :: Int -> [a] -> [[a]]
chop n [] = []
chop n xs = take n xs : chop n (drop n xs)

valid :: Grid -> Bool
valid g =   all nodups (rows g) &&
            all nodups (cols g) &&
            all nodups (boxs g)

nodups :: Eq a => [a] -> Bool
nodups [] = True
nodups (x:xs) = not (elem x xs) && nodups xs

solve :: Grid -> [Grid]
solve = filter valid . collapse . choices

choices :: Grid -> Matrix Choices
choices g = map (map choice) g
            where
                choice v = if v == '.' then ['1'..'9'] else [v]

cp :: [[a]] -> [[a]]
cp [] = [[]]
cp (xs:xss) = [y:ys | y <- xs, ys <- cp xss]

collapse :: Matrix [a] -> [Matrix a]
collapse m = cp $ map cp m

prune :: Matrix Choices -> Matrix Choices
prune = pruneBy boxs . pruneBy cols . pruneBy rows
        where pruneBy f = f . map reduce . f

reduce :: Row Choices -> Row Choices
reduce xss = [xs `minus` singles | xs <- xss]
            where singles = concat (filter single xss)

minus :: Choices -> Choices -> Choices
xs `minus` ys = if single xs then xs else xs \\ ys

single :: [a] -> Bool
single [_] = True
single _ = False

solve2 :: Grid -> [Grid]
solve2 = filter valid . collapse . prune . choices

solve3 :: Grid -> [Grid]
solve3 = filter valid . collapse . fix prune . choices

fix :: Eq a => (a -> a) -> a -> a
fix f x = if x == x' then x else fix f x'
            where x' = f x

void :: Matrix Choices -> Bool
void m = any (any null) m

safe :: Matrix Choices -> Bool
safe m = all consistent (rows m) &&
         all consistent (cols m) &&
         all consistent (boxs m)

consistent :: Row Choices -> Bool
consistent = nodups . concat . filter single

blocked :: Matrix Choices -> Bool
blocked m = void m || not (safe m)

solve4 :: Grid -> [Grid]
solve4 = search . prune . choices

search :: Matrix Choices -> [Grid]
search m | blocked m = []
         | all (all single) m = collapse m
         | otherwise = 
            [g | m' <- expand m, g <- search (prune m')]

expand :: Matrix Choices -> [Matrix Choices]
expand m =
    [rows1 ++ [row1 ++ [c] : row2] ++ rows2 | c <- cs]
    where
        (rows1,row:rows2) = break (any (not . single)) m
        (row1,cs:row2) = break (not . single) row